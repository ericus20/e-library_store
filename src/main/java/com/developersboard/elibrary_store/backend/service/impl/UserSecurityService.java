package com.developersboard.elibrary_store.backend.service.impl;

import com.developersboard.elibrary_store.backend.persistence.domain.User;
import com.developersboard.elibrary_store.backend.persistence.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

/**
 * Created by Eric on 2/9/2018.
 *
 * @author Eric Opoku
 */
@Slf4j
@Service
public class UserSecurityService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the <code>UserDetails</code>
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     *
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *                                   GrantedAuthority
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = null;
        // Ensure that username is not empty or not null.
        if (!StringUtils.isEmpty(username)) {

            user = userRepository.findByUsername(username);
            if (user == null) {
                LOG.warn("No record for user with username {} found", username);
                throw new UsernameNotFoundException("User with username " + username + " not found");
            }

        }
        return user;
    }
}
