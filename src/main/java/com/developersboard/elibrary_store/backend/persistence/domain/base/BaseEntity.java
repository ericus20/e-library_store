package com.developersboard.elibrary_store.backend.persistence.domain.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by Eric on 2/8/2018.
 *
 * This class Elevates the load of repeated declaration of id in all Entity classes.
 * Provides attributes used to audit changes made to files and versioning.
 *
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 * 
 */
@Data                   // Lombok: Auto generates getters and setters for the instance variables.
@ToString               // Lombok: Generates pretty toString.
@EqualsAndHashCode( of = {"version"} )  // Lombok: Generates equals and hashcode for the class using only version.
@MappedSuperclass       // This is a super class to be mapped by Entity classes.
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Version
    @Column(name = "version")
    protected int version;

    @Column(name = "created_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    protected Date createdAt;

    @CreatedBy
    protected String createdBy;

    @Column(name = "updated_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    protected Date updatedAt;

    @LastModifiedBy
    protected String modifiedBy;
}
