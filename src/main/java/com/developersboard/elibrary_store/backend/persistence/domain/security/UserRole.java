package com.developersboard.elibrary_store.backend.persistence.domain.security;

import com.developersboard.elibrary_store.backend.persistence.domain.User;
import com.developersboard.elibrary_store.backend.persistence.domain.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Eric on 2/9/2018.
 *
 * @author Eric
 */
@Data
@Entity
@NoArgsConstructor  // Creates a no argument constructor for the class.
@ToString( callSuper = true )
@EqualsAndHashCode( callSuper = false, of = {"role", "user"})
public class UserRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 992559513746153758L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;


    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }
}
