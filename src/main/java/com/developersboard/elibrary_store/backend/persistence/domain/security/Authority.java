package com.developersboard.elibrary_store.backend.persistence.domain.security;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by Eric on 2/9/2018.
 *
 * Authority Implements the Granted Authority from Spring Security.
 * This gives flexibility to us on how to modify this class.
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 */
public class Authority implements GrantedAuthority {

    private final String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    /**
     * If the <code>GrantedAuthority</code> can be represented as a <code>String</code>
     * and that <code>String</code> is sufficient in precision to be relied upon for an
     * access control decision by an {@link AccessDecisionManager} (or delegate), this
     * method should return such a <code>String</code>.
     * <p>
     * If the <code>GrantedAuthority</code> cannot be expressed with sufficient precision
     * as a <code>String</code>, <code>null</code> should be returned. Returning
     * <code>null</code> will require an <code>AccessDecisionManager</code> (or delegate)
     * to specifically support the <code>GrantedAuthority</code> implementation, so
     * returning <code>null</code> should be avoided unless actually required.
     *
     * @return a representation of the granted authority (or <code>null</code> if the
     * granted authority cannot be expressed as a <code>String</code> with sufficient
     * precision).
     */
    @Override
    public String getAuthority() {
        return this.authority;
    }
}
