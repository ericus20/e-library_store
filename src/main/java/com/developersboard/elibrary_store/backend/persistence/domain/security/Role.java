package com.developersboard.elibrary_store.backend.persistence.domain.security;

import com.developersboard.elibrary_store.enums.RolesEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Eric on 2/9/2018.
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 */
@Data
@Entity
@NoArgsConstructor
@ToString( exclude = {"userRoles"} )
@EqualsAndHashCode( of = {"id", "name"} )
public class Role implements Serializable {

    /** Since the object will be travelling across several JVM this UID will be to serialize and deserialize */
    private static final long serialVersionUID = 1858376306648592375L;

    /** Roles needed as at now is only 2 and no need to enable auto generation of Id. We will have 2 roles. */
    @Id
    private int id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<UserRole> userRoles = new HashSet<>();

    public Role(RolesEnum rolesEnum) {
        this.id = rolesEnum.getId();
        this.name = rolesEnum.getName();
    }
}
