package com.developersboard.elibrary_store.backend.persistence.domain;

import com.developersboard.elibrary_store.backend.persistence.domain.base.BaseEntity;
import com.developersboard.elibrary_store.backend.persistence.domain.security.Authority;
import com.developersboard.elibrary_store.backend.persistence.domain.security.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Eric on 2/8/2018.
 *
 * User Entity acquires its id from BaseEntity as well as auditing attributes.
 * User Entity implements UserDetails interface making it easier to use with Spring Granted Authorities
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 */
@Data
@Entity
@ToString( exclude = {"userRoles", "password"} ) // userRoles and password should not be printed with toString
@EqualsAndHashCode(callSuper = false, of = {"username", "email"}) // Use username and password for equals and hashcode
public class User extends BaseEntity implements Serializable, UserDetails {

    private static final long serialVersionUID = 9178661439383356177L;

    @NotEmpty(message = "Username cannot be left blank")
    @Column(unique = true)
    private String username;

    @NotEmpty(message = "Password cannot be left blank")
    private String password;

    private String firstName;

    @Column(name = "last_name", unique = true)
    private String lastName;

    @NotEmpty(message = "Email cannot be left blank")
    @Column(unique = true)
    private String email;

    @Column(name = "phone_number", unique = true)
    private String phoneNumber;

    private boolean enabled;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserRole> userRoles;

    @Column(name = "profile_image_url")
    private String profileImageUrl;

    /**
     * Returns the authorities granted to the user. Cannot return <code>null</code>.
     *
     * @return the authorities, sorted by natural key (never <code>null</code>)
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        Set<GrantedAuthority> authorities = new HashSet<>();

        // Iterate through all userRoles and add each role to the authorities. This will be used by Spring Security.
        userRoles.forEach(userRole -> authorities.add(new Authority(userRole.getRole().getName())));
        return authorities;
    }
}
