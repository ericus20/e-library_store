package com.developersboard.elibrary_store.backend.persistence.repositories;

import com.developersboard.elibrary_store.backend.persistence.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Eric on 2/9/2018.
 *
 * @author Eric Opoku
 */
@Repository
@Transactional(readOnly = true)
public interface UserRepository extends CrudRepository<User, Long> {

    /** Returns user that matches the specified username */
    User findByUsername(String username);
}
