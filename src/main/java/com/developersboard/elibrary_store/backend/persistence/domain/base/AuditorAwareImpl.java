package com.developersboard.elibrary_store.backend.persistence.domain.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by Eric on 2/8/2018.
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 */
@Slf4j
public class AuditorAwareImpl implements AuditorAware<String> {

    /**
     * Returns the current auditor of the application.
     *
     * @return the current auditor
     */
    @Override
    public String getCurrentAuditor() {

        // Check if there is a user logged in. If so, use the logged in user as the current auditor.
        // By default spring injects an anonymousUser if there is no authentication and authorization (user or admin)
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null
                && authentication.isAuthenticated()
                && authentication.getPrincipal() != "anonymousUser") {
            return authentication.getName();
        }

        // If there is no authentication, then the system will be used as the current auditor.
        return "system";
    }
}
