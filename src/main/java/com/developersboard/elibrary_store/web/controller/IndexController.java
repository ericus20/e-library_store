package com.developersboard.elibrary_store.web.controller;

import com.developersboard.elibrary_store.constants.ControllerConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Eric on 2/8/2018.
 *
 * Home Controller. The home for the root url "/".
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 *
 */
@Controller
public class IndexController {

    /**
     * Index mapping which is the root of the project.
     *
     * @return Index view name.
     */
    @GetMapping(value = ControllerConstants.INDEX_URL_MAPPING)
    public String index() {
        return ControllerConstants.INDEX_VIEW_NAME;
    }
}
