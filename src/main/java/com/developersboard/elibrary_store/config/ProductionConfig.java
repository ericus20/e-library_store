package com.developersboard.elibrary_store.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by Eric on 2/9/2018.
 *
 * @author Eric Opoku
 */
@Configuration
@Profile("prod")
@PropertySource("file:///${user.home}/.e-library_store/application-prod.properties")
public class ProductionConfig {
}
