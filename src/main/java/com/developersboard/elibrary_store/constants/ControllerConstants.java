package com.developersboard.elibrary_store.constants;

/**
 * Created by Eric on 2/8/2018.
 *
 * Constants used in controller classes are group here for consistency
 *
 * @author Eric Opoku
 * @author Matthew Puentes
 *
 */
public abstract class ControllerConstants {

    /** Ensures that instantiation of this abstract class is not permitted */
    private ControllerConstants() {
        throw new AssertionError("Non instantiable");
    }

    /** Url Mappings for various requests */
    public static final String INDEX_URL_MAPPING = "/";

    /** View Names for various html files */
    public static final String INDEX_VIEW_NAME = "index";
}
